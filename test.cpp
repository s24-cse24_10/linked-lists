#include <igloo/igloo.h>
#include <stdexcept>
#include <unistd.h>
#include "LinkedList.h"

using namespace igloo;

Context(TestLinkedList){
    int oldSTDOUT;
    int oldSTDERR;

    void SetUp() {
        oldSTDOUT = dup( 1 );
        oldSTDERR = dup( 2 );
        freopen( "/dev/null", "w", stdout );
        freopen( "/dev/null", "w", stderr );
    }

    void TearDown() {
        fflush( stdout );
        fflush( stderr );
        dup2( oldSTDOUT, 1 );
        dup2( oldSTDERR, 2 );
    }

    // Unit tests go here
    Spec(TestAppendEmpty) {
        LinkedList<int> list;
        list.append(1);

        Assert::That(list.head->data, Equals(1));
        Assert::That(list.head->next == nullptr, IsTrue());
    }

    Spec(TestAppendGeneral){
        LinkedList<int> list;
        list.append(1);
        list.append(2);
        list.append(3);

        Assert::That(list.head->data, Equals(1));
        Assert::That(list.head->next->data, Equals(2));
        Assert::That(list.head->next->next->data, Equals(3));
        Assert::That(list.head->next->next->next == nullptr, IsTrue());
    }

};


int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}