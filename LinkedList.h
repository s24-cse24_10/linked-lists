#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include<iostream>

template <class T>
struct Node {
    T data;
    Node<T>* next;

    Node() {
        data = {};
        next = nullptr;
    }

    Node(T data) {
        this->data = data;
        next = nullptr;
    }
};

template <class T>
struct LinkedList {
    Node<T>* head;
    Node<T>* tail;

    LinkedList() {
        head = nullptr;
        tail = nullptr;
    }

    void append(T data) {
        Node<T>* newNode = new Node<T>(data);
        if (head == nullptr) {
            head = newNode;
            tail = newNode;
        } else {
            tail->next = newNode;
            tail = newNode;
        }
    }

    T removeFirst() {
        if (head == nullptr) {
            throw std::logic_error("Empty list");
        } else {
            Node<T>* firstNode = head;
            T data = firstNode->data;

            if (head->next == nullptr) {
                head = nullptr;
                tail = nullptr;
            } else {
                head = head->next;
            }
            delete firstNode;

            return data;
        }
    }

    void print() {
        Node<int>* temp = head;
        std::cout << "head -> ";
        while (temp != nullptr) {
            std::cout << temp->data << " -> ";
            temp = temp->next;
        }
        std::cout << "null" << std::endl;
    }
};

#endif