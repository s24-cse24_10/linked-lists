#include <iostream>
#include "LinkedList.h"
using namespace std;



int main(int argc, char* argv[]) {

    LinkedList<int> list;

    list.append(7);
    list.append(12);
    list.append(42);
    list.print();

    cout << "removeFirst: " << list.removeFirst() << endl;
    list.print();

    // Node<int>* head = new Node<int>(7);
    // head->next = new Node<int>(12);
    // head->next->next = new Node<int>(42);



    return 0;
}